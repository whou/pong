#ifndef BALL_HPP
#define BALL_HPP

#include <cstdint>

#include "SDL.h"

#include "vec2.hpp"
#include "pad.hpp"

enum CollisionSide {
	NONE, 
	LEFT, 
	RIGHT, 
	TOP, 
	BOTTOM, 
};

class Ball {
public:
	Ball(Vec2 position, Vec2 direction);
	
	void draw(SDL_Renderer *context) const;
	void update(float delta_time);

	// adds the passed `Vec2` to its position.
	void move(Vec2 distance);
	// resets the ball to its spawn position (must be called after invert_dir)
	void reset();
	// invert ball direction
	void invert_dir();
	// invert ball direction in the x axis
	void invert_x_dir();
	// invert ball direction in the y axis
	void invert_y_dir();
	// check if it collides with a pad
	bool isPadColliding(Pad pad) const;
	// get which side a pad collision is happening
	CollisionSide getPadCollisionSide(Pad pad) const;

	Vec2 getPos() const;

	std::uint32_t size = 20;

private:
	Vec2 m_pos;
	Vec2 m_dir;
	Vec2 m_spawn;

	std::uint32_t velocity = 500;
	std::uint32_t y_axis_toggle = 0;
};

#endif // BALL_HPP
