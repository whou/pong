#include "ball.hpp"

#include <cstdint>

#include "vec2.hpp"
#include "pad.hpp"

Ball::Ball(Vec2 position, Vec2 direction) 
: m_pos(position - (size / 2)), 
  m_dir(direction), 
  m_spawn(m_pos)
{ }

void Ball::draw(SDL_Renderer *context) const {
	SDL_Rect rect = SDL_Rect {
		m_pos.x, m_pos.y, 
		static_cast<int>(size), static_cast<int>(size)
	};

	SDL_SetRenderDrawColor(context, 255, 255, 255, 255); // white
	SDL_RenderFillRect(context, &rect);
}

void Ball::update(float delta_time) {
	move(Vec2(velocity, velocity * y_axis_toggle) * m_dir * delta_time);
}

void Ball::move(Vec2 distance) {
	m_pos = m_pos + distance;
}

void Ball::reset() {
	m_pos = m_spawn;
	y_axis_toggle = 0;
}

void Ball::invert_dir() {
	m_dir = m_dir * -1;
	y_axis_toggle = 1;
}

void Ball::invert_x_dir() {
	m_dir.x *= -1;
	y_axis_toggle = 1;
}

void Ball::invert_y_dir() {
	m_dir.y *= -1;
	y_axis_toggle = 1;
}

bool Ball::isPadColliding(Pad pad) const {
	// simple AABB check
	return this->m_pos.x < pad.pos.x + pad.size.x &&
		   this->m_pos.x + static_cast<std::int32_t>(this->size) > pad.pos.x &&
		   this->m_pos.y < pad.pos.y + pad.size.y &&
		   this->m_pos.y + static_cast<std::int32_t>(this->size) > pad.pos.y;
}

CollisionSide Ball::getPadCollisionSide(Pad pad) const {
	if (!isPadColliding(pad)) {
		return CollisionSide::NONE;
	}
	
	const std::uint32_t half_size = this->size / 2;
	const Vec2 pad_half_size = pad.size / 2;
	const Vec2 center = Vec2(this->m_pos.x + half_size, this->m_pos.y + half_size);
	const Vec2 pad_center = Vec2(pad.pos.x + half_size, pad.pos.y + half_size);

	const Vec2 diff = Vec2(center.x - pad_center.x, center.y - pad_center.y);
	const Vec2 minDist = Vec2(half_size + pad_half_size.x, half_size + pad_half_size.y);
	Vec2 depth = Vec2(diff.x > 0 ? minDist.x - diff.x : -minDist.x - diff.x, 
	                  diff.y > 0 ? minDist.y - diff.y : -minDist.y - diff.y);

	if (depth != 0) {
		if (depth.x < depth.y) {
			return depth.x > 0 ? CollisionSide::LEFT : CollisionSide::RIGHT;
		} else {
			return depth.y > 0 ? CollisionSide::TOP : CollisionSide::BOTTOM;
		}
	}

	return CollisionSide::NONE;
}

Vec2 Ball::getPos() const {
	return m_pos;
}
