#ifndef PAD_HPP
#define PAD_HPP

#include <cstdint>

#include "SDL.h"

#include "font.hpp"
#include "vec2.hpp"

// TODO: optional mouse control
struct PadInput {
	SDL_Scancode up;
	SDL_Scancode down;

	PadInput(SDL_Scancode key_up, SDL_Scancode key_down) {
		this->up = key_up;
		this->down = key_down;
	}
};

class Pad {
public:
	Pad(Vec2 position, Vec2 score_board_position, PadInput input_keys);

	void draw(SDL_Renderer *context) const;
	void draw_score(SDL_Renderer *context, const Font *score_font) const;
	void update(float delta_time, const std::uint8_t *keys_state);

	Vec2 pos;
	Vec2 score_board;
	Vec2 size = Vec2(20, 150);
	
	std::uint32_t m_score;

private:
	PadInput m_inputkeys;

	std::uint32_t velocity = 500;
};

#endif // PAD_HPP
