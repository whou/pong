#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <cstdint>
#include <string>

#include "SDL.h"

#include "vec2.hpp"

class Renderer {
public:
	Renderer(const std::uint32_t width, const std::uint32_t height, const std::string title);
	~Renderer();
	
	// use this inside a loop checking for `Renderer::shouldClose()`
	void update();

	const std::uint8_t* getKeyboardState();
	SDL_Renderer* getContext();
	Vec2 getScreenSize();
	bool shouldClose();

private:
	struct context_t {
		SDL_Window *window = nullptr;
		SDL_Renderer *renderer = nullptr;
		SDL_Event event;
	} m_context;

	bool close = false;
	const std::uint8_t* m_keyboard_state;
};

#endif // RENDERER_HPP
