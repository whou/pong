#include "pad.hpp"

#include <cstdint>
#include <string>

#include "SDL.h"

#include "font.hpp"
#include "vec2.hpp"

Pad::Pad(Vec2 position, Vec2 score_board_position, PadInput input_keys) 
: pos(position), 
  score_board(score_board_position), 
  m_score(0), 
  m_inputkeys(input_keys)
{ }

void Pad::draw(SDL_Renderer *context) const {
	SDL_Rect rect = SDL_Rect {
		pos.x, pos.y, 
		size.x, size.y
	};

	SDL_SetRenderDrawColor(context, 255, 255, 255, 255); // white
	SDL_RenderFillRect(context, &rect);
}

void Pad::draw_score(SDL_Renderer *context, const Font *score_font) const {
	score_font->drawText(context, std::to_string(m_score), score_board);
}

void Pad::update(float delta_time, const std::uint8_t* keys_state) {
	if (keys_state[m_inputkeys.up]) {
		pos.y -= velocity * delta_time;
	} else if (keys_state[m_inputkeys.down]) {
		pos.y += velocity * delta_time;
	}
}
