#include "renderer.hpp"

#include <cstdint>
#include <iostream>
#include <string>

#include "SDL.h"

#include "vec2.hpp"
#include "font.hpp"

static void printSDLError(const std::string prev_msg) {
	std::cout << "ERROR: " << prev_msg << "\n";
	std::cout << "SDL Error: " << SDL_GetError() << "\n";
}

static void printTTFError(const std::string prev_msg) {
	std::cout << "ERROR: " << prev_msg << "\n";
	std::cout << "SDL_ttf Error: " << TTF_GetError() << "\n";
}

Renderer::Renderer(const std::uint32_t width, const std::uint32_t height, const std::string title) {
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printSDLError("SDL could not initialize.");
		close = true;
	}

	m_context.window = SDL_CreateWindow(title.c_str(), 
										SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
										width, height, 
										SDL_WINDOW_SHOWN);
	if (m_context.window == nullptr) {
		printSDLError("Window could not be created.");
		close = true;
	}

	m_context.renderer = SDL_CreateRenderer(m_context.window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (m_context.renderer == nullptr) {
		printSDLError("SDL Renderer could not be created.");
		close = true;
	}

	if (TTF_Init() < 0) {
		printTTFError("SDL_ttf could not initialize.");
	}

	m_keyboard_state = SDL_GetKeyboardState(nullptr);

	SDL_SetRenderDrawColor(m_context.renderer, 0, 0, 0, 255);
	SDL_RenderClear(m_context.renderer);
}

Renderer::~Renderer() {
	TTF_Quit();

	SDL_DestroyRenderer(m_context.renderer);
	m_context.renderer = nullptr;
	SDL_DestroyWindow(m_context.window);
	m_context.window = nullptr;

	SDL_Quit();
}

void Renderer::update() {
	SDL_RenderPresent(m_context.renderer);

	while (SDL_PollEvent(&m_context.event)) {
		if (m_context.event.type == SDL_QUIT) {
			close = true;
		}
		else if (m_context.event.type == SDL_KEYDOWN) {
			if (m_context.event.key.keysym.sym == SDLK_ESCAPE) {
				close = true;
			}
		}
	}

	// this is placed after SDL_RenderPresent because it is meant to clear
	// the screen before anything can be drawn in the next loop iteration.
	SDL_SetRenderDrawColor(m_context.renderer, 0, 0, 0, 255);
	SDL_RenderClear(m_context.renderer);
}

const std::uint8_t* Renderer::getKeyboardState() {
	return m_keyboard_state;
}

SDL_Renderer* Renderer::getContext() {
	return m_context.renderer;
}

Vec2 Renderer::getScreenSize() {
	Vec2 size = {0, 0};
	SDL_GetWindowSize(m_context.window, &size.x, &size.y);
	return size;
}

bool Renderer::shouldClose() {
	return close;
}
