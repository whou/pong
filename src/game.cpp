#include "game.hpp"

#include <cmath>
#include <cstdint>
#include <iostream>
#include <memory>
#include <string>

#include "SDL.h"
#include "SDL_timer.h"

#include "vec2.hpp"
#include "ball.hpp"
#include "font.hpp"

#define PLYR1_UP   SDL_SCANCODE_W
#define PLYR1_DOWN SDL_SCANCODE_S
#define PLYR2_UP   SDL_SCANCODE_UP
#define PLYR2_DOWN SDL_SCANCODE_DOWN

Game::Game(std::unique_ptr<Renderer> renderer, const std::uint32_t fps_cap) 
: m_renderptr(std::move(renderer)), 
  m_debugFont("bit5x3.ttf", 24), 
  m_scoreFont("bit5x3.ttf", 50), 
  m_ball(m_renderptr->getScreenSize() / 2, 1), 
  m_plyr1(0, 0, PadInput(PLYR1_UP, PLYR1_DOWN)), 
  m_plyr2(0, 0, PadInput(PLYR2_UP, PLYR2_DOWN)), 
  cap(fps_cap)
{
	m_timer.start();

	Vec2 screenSize = m_renderptr->getScreenSize();
	Vec2 screenCenter = m_renderptr->getScreenSize() / 2;
	const std::int32_t padWallDistance = 10;

	// start point on the left side
	m_plyr1.pos.x = padWallDistance;
	m_plyr1.pos.y = screenCenter.y - (m_plyr1.size.y / 2);
	m_plyr1.score_board.x = screenCenter.x - 75;
	m_plyr1.score_board.y = 75;
	// start point on the right side - offset with screen size
	m_plyr2.pos.x = screenSize.x - (m_plyr2.size.x + padWallDistance);
	m_plyr2.pos.y = screenCenter.y - (m_plyr2.size.y / 2);
	m_plyr2.score_board.x = screenCenter.x + 75;
	m_plyr2.score_board.y = 75;
}

void Game::run() {
	while (!m_renderptr->shouldClose()) {
		float delta_time = (SDL_GetTicks() - m_ticksCount) / 1000.0f;
		m_ticksCount = SDL_GetTicks();

		// correct and calculate FPS
		fps = fps < 2000000 ? frames / (m_timer.getTicks() / 1000.0f) : 0;

		update(delta_time);
		draw();

		m_renderptr->update();
		++frames;
	}
}

void Game::draw() const {
	m_ball.draw(m_renderptr->getContext());
	m_plyr1.draw(m_renderptr->getContext());
	m_plyr2.draw(m_renderptr->getContext());

	m_plyr1.draw_score(m_renderptr->getContext(), &m_scoreFont);
	m_plyr2.draw_score(m_renderptr->getContext(), &m_scoreFont);

	#ifdef DEBUG
	m_debugFont.drawText(m_renderptr->getContext(), std::to_string(fps), Vec2(0, 0));
	#endif
}

void Game::update(float delta_time) {
	CollisionSide side = m_ball.getPadCollisionSide(m_plyr1);
	if (side == CollisionSide::NONE) {
		side = m_ball.getPadCollisionSide(m_plyr2);
	}

	switch (side) {
		case NONE:
			break;
		case LEFT:
		case RIGHT:
			m_ball.invert_x_dir();
			break;
		case TOP:
		case BOTTOM:
			m_ball.invert_dir();
	}

	if (m_ball.getPos().y <= 0 || 
	    m_ball.getPos().y + static_cast<std::int32_t>(m_ball.size) >= m_renderptr->getScreenSize().y) 
	{
		m_ball.invert_y_dir();
	}

	if (m_ball.getPos().x + static_cast<std::int32_t>(m_ball.size) >= m_renderptr->getScreenSize().x) {
		m_plyr1.m_score++;
		m_ball.invert_dir();
		m_ball.reset();
	} else if (m_ball.getPos().x <= 0) {
		m_plyr2.m_score++;
		m_ball.invert_dir();
		m_ball.reset();
	}

	m_ball.update(delta_time);
	m_plyr1.update(delta_time, m_renderptr->getKeyboardState());
	m_plyr2.update(delta_time, m_renderptr->getKeyboardState());
}
