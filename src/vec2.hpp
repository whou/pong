#ifndef VEC2_HPP
#define VEC2_HPP

#include <cstdint>

struct Vec2 {
	std::int32_t x;
	std::int32_t y;

	Vec2(std::int32_t x, std::int32_t y) {
		this->x = x;
		this->y = y;
	}

	Vec2(std::int32_t value) {
		this->x = value;
		this->y = value;
	}

	bool operator== (const Vec2& vec2) {
		return this->x == vec2.x && this->y == vec2.y;
	}

	bool operator!= (const Vec2& vec2) {
		return this->x != vec2.x && this->y != vec2.y;
	}

	Vec2& operator+ (const Vec2& vec2) {
		x += vec2.x;
		y += vec2.y;
		return *this;
	}

	Vec2& operator- (const Vec2& vec2) {
		x -= vec2.x;
		y -= vec2.y;
		return *this;
	}

	Vec2& operator* (const Vec2& vec2) {
		x *= vec2.x;
		y *= vec2.y;
		return *this;
	}

	Vec2& operator/ (const Vec2& vec2) {
		x /= vec2.x;
		y /= vec2.y;
		return *this;
	}

	template<typename T>
	bool operator== (const T& value) {
		return this->x == value && this->y == value;
	}

	template<typename T>
	bool operator!= (const T& value) {
		return this->x != value && this->y != value;
	}

	template<typename T>
	Vec2& operator+ (const T& value) {
		x += value;
		y += value;
		return *this;
	}

	template<typename T>
	Vec2& operator- (const T& value) {
		x -= value;
		y -= value;
		return *this;
	}

	template<typename T>
	Vec2& operator* (const T& value) {
		x *= value;
		y *= value;
		return *this;
	}

	template<typename T>
	Vec2& operator/ (const T& value) {
		x /= value;
		y /= value;
		return *this;
	}
};

#endif // VEC2_HPP
