#include <memory>

#include "SDL.h"

#include "renderer.hpp"
#include "game.hpp"

#define WINDOW_WIDTH  960
#define WINDOW_HEIGHT 540
#define WINDOW_TITLE  "Pong!"
#define FPS_CAP       60

int main(int, char**) {
	std::unique_ptr<Renderer> renderer(new Renderer(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE));
	Game game = Game(std::move(renderer), FPS_CAP);

	game.run();

	return 0;
}
