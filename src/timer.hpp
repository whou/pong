#ifndef TIMER_HPP
#define TIMER_HPP

#include <cstdint>

class Timer {
public:
	void start();
	void stop();
	void pause();
	void unpause();

	std::uint32_t getTicks();

	bool isStarted();
	bool isPaused();

private:
	// clock time when the timer started
	std::uint32_t m_startTicks = 0;
	std::uint32_t m_pausedTicks = 0;

	bool paused = false;
	bool started = false;
};

#endif // TIMER_HPP
