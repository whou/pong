#include "timer.hpp"

#include <cstdint>

#include "SDL.h"

void Timer::start() {
	started = true;
	paused = false;

	m_startTicks = SDL_GetTicks();
	m_pausedTicks = 0;
}

void Timer::stop() {
	started = false;
	paused = false;

	m_startTicks = 0;
	m_pausedTicks = 0;
}

void Timer::pause() {
	if (started && !paused) {
		paused = true;

		m_pausedTicks = SDL_GetTicks() - m_startTicks;
		m_startTicks = 0;
	}
}

void Timer::unpause() {
	if (started && paused) {
		paused = false;

		m_startTicks = SDL_GetTicks() - m_pausedTicks;
		m_pausedTicks = 0;
	}
}

std::uint32_t Timer::getTicks() {
	if (started) {
		if (paused) {
			return m_pausedTicks;
		}

		return SDL_GetTicks() - m_startTicks;
	}

	return 0;
}
