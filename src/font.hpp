#ifndef FONT_HPP
#define FONT_HPP

#include <cstdint>
#include <string>
#include <vector>

#include "SDL.h"
#include "SDL_ttf.h"

#include "vec2.hpp"

class Font {
public:
	Font(const std::string & path, const std::uint32_t size);
	~Font();

	void drawText(SDL_Renderer *context, const std::string & text, Vec2 position) const;

private:
	std::vector<char> bytes;
	void *membuf;
	TTF_Font *font;
};

#endif // FONT_HPP
