#include "font.hpp"

#include <cstdlib>
#include <cstdint>
#include <stdexcept>
#include <string>
#include <vector>

#include <cmrc/cmrc.hpp>
#include "SDL.h"
#include "SDL_ttf.h"

#include "vec2.hpp"

CMRC_DECLARE(font);

Font::Font(const std::string & path, const std::uint32_t size) {
	const auto file = cmrc::font::get_filesystem().open(path);
	bytes = std::vector<char>(file.begin(), file.end());

	std::int64_t bufSize;
	{
		auto buffer = SDL_RWFromConstMem(bytes.data(), bytes.size());
		if (!buffer) {
			throw std::runtime_error(std::string("Unable to load \"" + path + "\" from memory"));
		}

		bufSize = SDL_RWsize(buffer);
		membuf = std::malloc(bufSize);
		SDL_RWread(buffer, membuf, 1, bufSize);
		SDL_RWclose(buffer);
	}

	auto fontBuffer = SDL_RWFromConstMem(membuf, bufSize);
	font = TTF_OpenFontRW(fontBuffer, 1, size);

	if (!font) {
		throw std::runtime_error(std::string("Unable to load \"" + path + "\" as a font"));
	}
}

Font::~Font() {
	TTF_CloseFont(font);
	std::free(membuf);
}

void Font::drawText(SDL_Renderer *context, const std::string & text, Vec2 position) const {
	SDL_Surface *surface = TTF_RenderText_Solid(font, text.c_str(), SDL_Color{255, 255, 255, 255});
	SDL_Texture *texture = SDL_CreateTextureFromSurface(context, surface);

	SDL_Rect rect = SDL_Rect{
		position.x, position.y, 
		surface->w, surface->h
	};

	SDL_RenderCopy(context, texture, nullptr, &rect);

	SDL_FreeSurface(surface);
	SDL_DestroyTexture(texture);
}
