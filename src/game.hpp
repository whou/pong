#ifndef GAME_HPP
#define GAME_HPP

#include <memory>
#include <cstdint>

#include "renderer.hpp"
#include "ball.hpp"
#include "pad.hpp"
#include "timer.hpp"
#include "font.hpp"

class Game {
public:
	Game(std::unique_ptr<Renderer> renderer, const std::uint32_t fps_cap);

	// doesn't return until the game window is closed.
	void run();

private:
	std::unique_ptr<Renderer> m_renderptr;
	Font m_debugFont;
	Font m_scoreFont;
	Ball m_ball;
	Pad m_plyr1;
	Pad m_plyr2;

	void draw() const;
	void update(float delta_time);

	Timer m_timer;
	std::uint32_t m_ticksCount = 0;

	std::int32_t frames = 0;
	std::int32_t fps = 0;
	const std::uint32_t cap;
};

#endif // GAME_HPP
